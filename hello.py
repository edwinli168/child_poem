from flask import Flask
import tensorflow as tf
import numpy as np
import argparse
import os
import random
import time
import collections
import xlrd

batchSize = 64

learningRateBase = 0.001
learningRateDecayStep = 1000
learningRateDecayRate = 0.95

epochNum = 10                    # train epoch
generateNum = 5                   # number of generated poems per time

type = "child"                   # dataset to use, shijing, songci, etc
trainPoems = "./dataset/" + type + "/" + type + ".xlsx"  # training file location
checkpointsPath = "./checkpoints/" + type  # checkpoints location

saveStep = 1000                   # save model every savestep


# evaluate
trainRatio = 0.8                    # train percentage
evaluateCheckpointsPath = "./checkpoints/evaluate"


class POEMS:
    "poem class"

    def __init__(self, filename, isEvaluate=False):
        """pretreatment"""

        poems = []

        workbook = xlrd.open_workbook(filename)
        table = workbook.sheet_by_index(0)
        nrows = table.nrows
        for rownum in range(0, nrows):
            row = table.row_values(rownum)
            if row:
                poem = row[3]
                poem = '[' + poem + ']'
                poems.append(poem)

        #counting words
        wordFreq = collections.Counter()
        for poem in poems:
            wordFreq.update(poem)
        # print(wordFreq)

        # erase words which are not common
        #--------------------bug-------------------------
        # word num less than original num, which causes nan value in loss function
        # erase = []
        # for key in wordFreq:
        #     if wordFreq[key] < 2:
        #         erase.append(key)
        # for key in erase:
        #     del wordFreq[key]

        wordFreq[" "] = -1
        wordPairs = sorted(wordFreq.items(), key=lambda x: -x[1])
        self.words, freq = zip(*wordPairs)
        self.wordNum = len(self.words)

        self.wordToID = dict(zip(self.words, range(self.wordNum)))  # word to ID
        poemsVector = [([self.wordToID[word] for word in poem]) for poem in poems]  # poem to vector
        if isEvaluate:  # evaluating need divide dataset into test set and train set
            self.trainVector = poemsVector[:int(len(poemsVector) * trainRatio)]
            self.testVector = poemsVector[int(len(poemsVector) * trainRatio):]
        else:
            self.trainVector = poemsVector
            self.testVector = []
        print("train sample total: %d" % len(self.trainVector))
        print("test sample total: %d" % len(self.testVector))

    def generateBatch(self, isTrain=True):
        #padding length to batchMaxLength
        if isTrain:
            poemsVector = self.trainVector
        else:
            poemsVector = self.testVector

        random.shuffle(poemsVector)
        batchNum = (len(poemsVector) - 1) // batchSize
        X = []
        Y = []
        #create batch
        for i in range(batchNum):
            batch = poemsVector[i * batchSize: (i + 1) * batchSize]
            maxLength = max([len(vector) for vector in batch])
            temp = np.full((batchSize, maxLength), self.wordToID[" "], np.int32)  # padding space
            for j in range(batchSize):
                temp[j, :len(batch[j])] = batch[j]
            X.append(temp)
            temp2 = np.copy(temp)  # copy!!!!!!
            temp2[:, :-1] = temp[:, 1:]
            Y.append(temp2)
        return X, Y


class MODEL:
    """model class"""

    def __init__(self, trainData):
        self.trainData = trainData

    def buildModel(self, wordNum, gtX, hidden_units=128, layers=2):
        """build rnn"""
        with tf.variable_scope("embedding"):  # embedding
            embedding = tf.get_variable(
                "embedding", [wordNum, hidden_units], dtype=tf.float32)
            inputbatch = tf.nn.embedding_lookup(embedding, gtX)

        basicCell = tf.contrib.rnn.BasicLSTMCell(
            hidden_units, state_is_tuple=True)
        stackCell = tf.contrib.rnn.MultiRNNCell([basicCell] * layers)
        initState = stackCell.zero_state(np.shape(gtX)[0], tf.float32)
        outputs, finalState = tf.nn.dynamic_rnn(
            stackCell, inputbatch, initial_state=initState)
        outputs = tf.reshape(outputs, [-1, hidden_units])

        with tf.variable_scope("softmax"):
            w = tf.get_variable("w", [hidden_units, wordNum])
            b = tf.get_variable("b", [wordNum])
            logits = tf.matmul(outputs, w) + b

        probs = tf.nn.softmax(logits)
        return logits, probs, stackCell, initState, finalState

    def train(self, reload=True):
        """train model"""
        print("training...")
        gtX = tf.placeholder(tf.int32, shape=[batchSize, None])  # input
        gtY = tf.placeholder(tf.int32, shape=[batchSize, None])  # output

        logits, probs, a, b, c = self.buildModel(self.trainData.wordNum, gtX)

        targets = tf.reshape(gtY, [-1])

        #loss
        loss = tf.contrib.legacy_seq2seq.sequence_loss_by_example([logits], [targets], [tf.ones_like(targets, dtype=tf.float32)])
        globalStep = tf.Variable(0, trainable=False)
        addGlobalStep = globalStep.assign_add(1)

        cost = tf.reduce_mean(loss)
        trainableVariables = tf.trainable_variables()
        # prevent loss divergence caused by gradient explosion
        grads, a = tf.clip_by_global_norm(
            tf.gradients(cost, trainableVariables), 5)
        learningRate = tf.train.exponential_decay(learningRateBase, global_step=globalStep, decay_steps=learningRateDecayStep, decay_rate=learningRateDecayRate)
        optimizer = tf.train.AdamOptimizer(learningRate)
        trainOP = optimizer.apply_gradients(zip(grads, trainableVariables))

        with tf.Session() as sess:
            sess.run(tf.global_variables_initializer())
            saver = tf.train.Saver()

            if not os.path.exists(checkpointsPath):
                os.mkdir(checkpointsPath)

            if reload:
                checkPoint = tf.train.get_checkpoint_state(checkpointsPath)
                # if have checkPoint, restore checkPoint
                if checkPoint and checkPoint.model_checkpoint_path:
                    saver.restore(sess, checkPoint.model_checkpoint_path)
                    print("restored %s" % checkPoint.model_checkpoint_path)
                else:
                    print("no checkpoint found!")

            for epoch in range(epochNum):
                X, Y = self.trainData.generateBatch()
                epochSteps = len(X)  # equal to batch
                for step, (x, y) in enumerate(zip(X, Y)):
                    a, loss, gStep = sess.run(
                        [trainOP, cost, addGlobalStep], feed_dict={gtX: x, gtY: y})
                    print("epoch: %d, steps: %d/%d, loss: %3f" % (epoch + 1, step + 1, epochSteps, loss))
                    if gStep % saveStep == saveStep - 1:  # prevent save at the beginning
                        print("save model")
                        saver.save(sess, os.path.join(checkpointsPath, type), global_step=gStep)

    def probsToWord(self, weights, words):
        """probs to word"""
        prefixSum = np.cumsum(weights)  # prefix sum
        ratio = np.random.rand(1)
        # large margin has high possibility to be sampled
        index = np.searchsorted(prefixSum, ratio * prefixSum[-1])
        return words[index[0]]

    def test(self):
        """write regular poem"""
        print("genrating...")
        gtX = tf.placeholder(tf.int32, shape=[1, None])  # input
        logits, probs, stackCell, initState, finalState = self.buildModel(self.trainData.wordNum, gtX)
        with tf.Session() as sess:
            sess.run(tf.global_variables_initializer())
            saver = tf.train.Saver()
            checkPoint = tf.train.get_checkpoint_state(checkpointsPath)
            # if have checkPoint, restore checkPoint
            if checkPoint and checkPoint.model_checkpoint_path:
                saver.restore(sess, checkPoint.model_checkpoint_path)
                print("restored %s" % checkPoint.model_checkpoint_path)
            else:
                print("no checkpoint found!")
                exit(1)
            poems = []
            for i in range(generateNum):
                state = sess.run(stackCell.zero_state(1, tf.float32))
                # init start sign
                x = np.array([[self.trainData.wordToID['[']]])
                probs1, state = sess.run([probs, finalState], feed_dict={gtX: x, initState: state})
                word = self.probsToWord(probs1, self.trainData.words)
                poem = ''
                sentenceNum = 0
                while word not in [' ', ']']:
                    poem += word
                    if word in ['.', '?', '!', ',']:
                        sentenceNum += 1
                        if sentenceNum % 2 == 0:
                            poem += '\n'
                    x = np.array([[self.trainData.wordToID[word]]])
                    #print(word)
                    probs2, state = sess.run([probs, finalState], feed_dict={gtX: x, initState: state})
                    word = self.probsToWord(probs2, self.trainData.words)
                print(poem)
                poems.append(poem)
            return poems

    def testHead(self, characters):
        """write head poem"""
        print("genrating...")
        gtX = tf.placeholder(tf.int32, shape=[1, None])  # input
        logits, probs, stackCell, initState, finalState = self.buildModel(
            self.trainData.wordNum, gtX)
        with tf.Session() as sess:
            sess.run(tf.global_variables_initializer())
            saver = tf.train.Saver()
            checkPoint = tf.train.get_checkpoint_state(checkpointsPath)
            # if have checkPoint, restore checkPoint
            if checkPoint and checkPoint.model_checkpoint_path:
                saver.restore(sess, checkPoint.model_checkpoint_path)
                print("restored %s" % checkPoint.model_checkpoint_path)
            else:
                print("no checkpoint found!")
                exit(1)
            flag = 1
            endSign = {-1: ",", 1: "."}
            poem = ''
            state = sess.run(stackCell.zero_state(1, tf.float32))
            x = np.array([[self.trainData.wordToID['[']]])
            probs1, state = sess.run([probs, finalState], feed_dict={gtX: x, initState: state})
            for word in characters:
                if self.trainData.wordToID.get(word) == None:
                    print("Do not know this word!")
                    exit(0)
                flag = -flag
                while word not in [']', ',', '.', ' ', '?', '!']:
                    poem += word
                    x = np.array([[self.trainData.wordToID[word]]])
                    probs2, state = sess.run([probs, finalState], feed_dict={gtX: x, initState: state})
                    word = self.probsToWord(probs2, self.trainData.words)

                poem += endSign[flag]
                # keep the context, state must be updated
                if endSign[flag] == '.':
                    probs2, state = sess.run([probs, finalState],feed_dict={gtX: np.array([[self.trainData.wordToID["."]]]), initState: state})
                    poem += '\n'
                else:
                    probs2, state = sess.run([probs, finalState],feed_dict={gtX: np.array([[self.trainData.wordToID[","]]]), initState: state})

            print(characters)
            print(poem)
            return poem


trainData = POEMS(trainPoems)
MCPangHu = MODEL(trainData)


app = Flask(__name__)

@app.route("/")
def hello():
    newpoem = MCPangHu.test()
    print(newpoem)
    return '<p>new poem</p>'.join(newpoem)

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=8080)
